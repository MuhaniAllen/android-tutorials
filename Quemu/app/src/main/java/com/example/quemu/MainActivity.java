 package com.example.quemu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

 public class MainActivity extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

//    yellow = o; red = 1
    boolean gameActive;
    Integer activePalyer;

    int[] gameState;
    int[][] winningPositons = {{0,1,2},{3,4,5},{6,7,8},
                                {0,3,6},{1,4,3}, {2,5,8},
                                {0,4,8}, {2,4,6}};
    Button restart;
    TextView winnerTxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        restart = findViewById(R.id.restart);
        restart.setEnabled(false);
        winnerTxt = findViewById(R.id.winner);
       reset(this.getWindow().getDecorView());

//  todo      disable the button

    }

     public void dropIn(View view) {

         ImageView counter  = (ImageView) view;


             int tappedCounter = Integer.parseInt(counter.getTag().toString());
         if (gameActive) {

             if (gameState[tappedCounter] ==2) {
                 counter.setTranslationY(-1500);
                 gameState[tappedCounter] = activePalyer;
                 String winner;
                 if (activePalyer == 0) {
                     counter.setImageResource(R.drawable.yellow);
                     activePalyer = 1;
                     winner = "yellow";
                 } else {
                     counter.setImageResource(R.drawable.red);
                     winner = "red";
                     activePalyer = 0;
                 }
                 for (int[] winningPosition : winningPositons) {

                     if (gameState[winningPosition[0]] == gameState[winningPosition[1]]
                             && gameState[winningPosition[1]] == gameState[winningPosition[2]]
                             && gameState[winningPosition[0]] != 2 && gameActive) {
                         restart.setVisibility(View.VISIBLE);
                         gameActive = false;
                         winnerTxt.setText("Player " + winner + " one has won");
                         winnerTxt.setVisibility(View.VISIBLE);
                         restart.setEnabled(true);

                         Toast.makeText(this, "Player " + winner + " one has won", Toast.LENGTH_SHORT).show();
                     }
                 }


                 counter.animate().translationYBy(1500).rotation(3600).setDuration(300);
                 counter.animate().rotation(3600).setDuration(1000);
                 Log.i(TAG, "drop in button clicked");
                 Log.i(TAG, counter.getTag().toString());
             }
         }else {
             Toast.makeText(this, "Game over", Toast.LENGTH_SHORT).show();
         }

     }

     public void restartGame(View view) {
         reset(view);
     }

     public void reset(View view){
         gameState = new int[]{2, 2, 2,
                 2, 2, 2,
                 2, 2, 2};
         gameActive = true;
         activePalyer = 0;
         restart.setEnabled(false);
         androidx.gridlayout.widget.GridLayout gridView = findViewById(R.id.gridView);
         for (int i=0; i <gridView.getChildCount(); i++){
             ImageView img = (ImageView) gridView.getChildAt(i);
             img.setImageResource(0);
         }
     }
 }
