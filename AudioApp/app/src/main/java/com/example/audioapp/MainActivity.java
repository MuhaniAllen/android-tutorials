package com.example.audioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    String TAG = this.getClass().getSimpleName();
    AudioManager audioManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       mediaPlayer = MediaPlayer.create(this, R.raw.test2);
       audioManager= (AudioManager) getSystemService(AUDIO_SERVICE);
       int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
       int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        SeekBar vlmCtrl = findViewById(R.id.volumeCtrl);
        vlmCtrl.setMax(maxVolume);
        vlmCtrl.setProgress(currentVolume);

        vlmCtrl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.i(TAG,"Seekbar value has been altered     ======"+i);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i,0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });





        final SeekBar scrabSeekbar = findViewById(R.id.scrabCtrl);
        scrabSeekbar.setMax(mediaPlayer.getDuration());
        scrabSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.i(TAG, "Scrab controll used ======" +i);
                mediaPlayer.seekTo(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mediaPlayer.pause();

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.start();

            }
        });

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                scrabSeekbar.setProgress(mediaPlayer.getCurrentPosition());
            }
        }, 0,1200);
    }



    public void play(View view) {
        if (!mediaPlayer.isPlaying()){
        mediaPlayer.start();
        }
    }

    public void pause(View view) {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }
}
